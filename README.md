CMAKE toolchains
----

These is a collection of CMAKE toolchains that I'm using most.
Currently, only cortex_m3 is tested.

### How to use:

##### Windows
To use these toolchains you need to do the following steps:
- Install cmake for Windows from [here](https://cmake.org/download/)
- Download the make Windows binaries from [here](http://gnuwin32.sourceforge.net/packages/make.htm) and extract them to your C:\ drive.
> e.g. C:\opt\make

- Download the make dependencies from [here](http://downloads.sourceforge.net/project/gnuwin32/make/3.81/make-3.81-dep.zip) and extract them to same folder where make is.
- Download a gcc bare metal compiler for ARM from [here](https://launchpad.net/gcc-arm-embedded/+download) (e.g. gcc-arm-none-eabi-4_8-2014q1) and install it to your C:\ drive
> e.g. C:\opt\gcc-arm-none-eabi-4_8-2014q1

- Add the appropriate paths for gcc, cmake, and make to you system environment variables
> e.g. C:\Program Files\CMake\bin;C:\opt\gcc-arm-none-eabi-4_8-2014q1\bin;C:\opt\make\bin

- In the TOOLCHAIN_arm_none_eabi_cortex_mX.cmake file change the TOOLCHAIN_DIR path to correspond to your GCC path
> e.g. set(TOOLCHAIN_DIR C:\Program Files\CMake\bin;C:\opt\gcc-arm-none-eabi-4_8-2014q1\bin)

##### Linux
To use these toolchains you need to do the following steps:
- install cmake 2.8 for your distro
- install make for your distro
- download and extract a bare metal ARM compiler from [here](https://launchpad.net/gcc-arm-embedded/+download) (e.g. gcc-arm-none-eabi-4_8-2014q1) to your home folder or in /opt/
- In the TOOLCHAIN_arm_none_eabi_cortex_mX.cmake file change the TOOLCHAIN_DIR path to correspond to your GCC path
> e.g. set(TOOLCHAIN_DIR /opt/gcc-arm-none-eabi-4_8-2014q1/)

### ARM (bare metal)
These cmake files can be used to build bare metal code for several ARM processors including STM32 & NXP LPC

##### Cortex-M0
TOOLCHAIN_arm_none_eabi_cortex_m0.cmake

##### Cortex-M3
TOOLCHAIN_arm_none_eabi_cortex_m3.cmake

##### Cortex-M4
TOOLCHAIN_arm_none_eabi_cortex_m4.cmake
